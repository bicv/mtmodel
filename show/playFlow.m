% Show optical flow stream superimposed on original stimulus
function playFlow(s, flow, pauseLength)
  % Assign defaults
  displayRange = [min2(s), max2(s)];
  if nargin < 3
    pauseLength = 0;
  end
  % Play stimulus
  T = size(s, 3);
  for t = 1:T
    %hold on
    %showIm(s(:, :, i), displayRange, 'auto', 0);
    if t < T
      plotFlow(flow(:, :, :, t));
    end
    set(gca,'YDir','reverse');
    drawnow
    %hold off
    pause(pauseLength);
  end
end