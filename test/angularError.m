% Compute the angular error between estimated flow and ground truth
function ae = angularError(flow, flowGT)
  % Extract vectors
  u = flow(:, :, 1);
  v = flow(:, :, 2);
  uGT = flowGT(:, :, 1);
  vGT = flowGT(:, :, 2);
  % Calculate error (in degrees)
  ae = acosd((1 + u .* uGT + v .* vGT) ./ (sqrt(1 + u.^2 + v.^2) .* sqrt(1 + uGT.^2 + vGT.^2)));
end