% Infer a stimuli using a population vector code
function vel = popVector(resp, Beta)
  vel = resp * Beta;
end