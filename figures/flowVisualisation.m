function flowVisualisation(ground, MT, LK, WWLK, isRelative)
  maxFlow = -1;
  % If relative to ground, get flow range
  if isRelative
    mag = sqrt(ground(:, :, 1).^2 + ground(:, :, 2).^2);
    maxFlow = max(mag(:));
    velocityRange = -maxFlow:maxFlow/50:maxFlow;
  else
    velocityRange = -6:6/50:6;
  end
  % Plot colourwheel
  [X, Y] = meshgrid(velocityRange);
  col = zeros(length(velocityRange), length(velocityRange), 2);
  col(:, :, 1) = X;
  col(:, :, 2) = Y;
  subaxis(5, 5, 13, 'Margin', 0.08, 'SpacingVert', 0, 'SpacingHoriz', 0)
  subimage(flowToColor(col, maxFlow))
  axis off
  % Plot flow
  subaxis(5, 5, [1:2 6:7])
  subimage(flowToColor(ground, maxFlow))
  title('a', 'FontSize', 12, 'Color', 'white')
  axis off
  subaxis(5, 5, [4:5 9:10])
  subimage(flowToColor(MT, maxFlow))
  title('b', 'FontSize', 12, 'Color', 'white')
  axis off
  subaxis(5, 5, [16:17 21:22])
  subimage(flowToColor(LK, maxFlow))
  title('c', 'FontSize', 12, 'Color', 'white')
  axis off
  subaxis(5, 5, [19:20 24:25])
  subimage(flowToColor(WWLK, maxFlow))
  title('d', 'FontSize', 12, 'Color', 'white')
  axis off
  set(gcf, 'Color', 'k')
  export_fig -m3 -nocrop colourVis.png
end