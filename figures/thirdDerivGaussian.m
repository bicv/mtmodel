mu = 0;
sigma = 1;
x = -5:0.05:5;
n = (exp(-(x-mu).^2./(2*sigma^2)).*(x-mu).*(x.^2-2*x.*mu+mu^2-3*sigma^2))./(sqrt(2*pi)*sigma^7);
g = exp(-(x-mu).^2).*exp(-i*(x-mu));
figure
subplot(1, 2, 1)
plot(x, n, 'k', 'LineWidth', 1.5)
axis([-5 5 -0.6 0.6])
axis square
title('a', 'FontSize', 14)
xlabel('x', 'FontSize', 12)
ylabel('3rd Derivative of Gaussian(x)', 'FontSize', 12)
subplot(1, 2, 2)
plot(x, imag(g), 'k', 'LineWidth', 1.5)
axis([-5 5 -0.6 0.6])
axis square
title('b', 'FontSize', 14)
xlabel('x', 'FontSize', 12)
ylabel('Imaginary Part of Gabor(x)', 'FontSize', 12)
set(gcf, 'Color', 'w')
export_fig thirdDerivGaussian.png