pars = shPars;
pars.v1SpatialFilters = pars.v1SpatialFilters(2:8, :);
pars.v1TemporalFilters = pars.v1TemporalFilters(2:8, :);
[filter, S] = shMkV1Filter(pars, [0 1]);
scale = 128/max(filter(:));
filter = filter * scale + 128;
for i = 1:7
  subaxis(1, 8, i, 'Margin', 0.08, 'SpacingVert', 0, 'SpacingHoriz', 0.01)
  image(filter(:, :, i))
  colormap(gray(256))
  axis off
  axis square
end
set(gcf, 'Color', 'w')
export_fig -m3 v1Filter.png