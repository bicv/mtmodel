function blandAltmanPlot(A,B) 
  meanAB = (A + B)./2;
  difff = A - B;
  meanDiff = mean(difff);
  stdDiff = std(difff);
  
  meanp2D = meanDiff + 2*stdDiff;
  meanm2D = meanDiff - 2*stdDiff;
  minD = min(meanAB) - 0.1;
  maxD = max(meanAB) + 0.1;
  
  figure
  plot(meanAB, difff, '.k', 'MarkerSize', 20)
  hold on
  plot([minD; maxD], ones(1, 2)*meanp2D, '--k')
  text(minD + 2, meanp2D - 3, 'Mean + 2*SD', 'FontSize', 12)
  hold on
  plot([minD; maxD], ones(1, 2)*meanm2D, '--k')
  text(minD + 2, meanm2D + 3, 'Mean - 2*SD', 'FontSize', 12)
  hold on
  plot([minD; maxD], ones(1, 2)*meanDiff, '--k')
  xlim([minD maxD])
  xlabel('(L + M) / 2', 'FontSize', 12)
  ylabel('L - M', 'FontSize', 12)
  set(gcf, 'color', 'w')
end