% (requires training data and Beta)
p = XTrain * Beta;
[mags, i] = sort(sqrt(sum(yTrain.^2, 2)));
% Plot x residuals
subplot(1, 2, 1)
plot([0 100], [0 0], '-k')
hold on
plot(mags, yTrain(i, 1) - p(i, 1), '.k')
hold off
xlim([0 6])
ylim([-0.15 0.15])
ylabel('residual', 'FontSize', 12)
xlabel('magnitude', 'FontSize', 12)
title('a', 'FontSize', 14)
% Plot y residuals
subplot(1, 2, 2)
plot([0 100], [0 0], '-k')
hold on
plot(mags, yTrain(i, 2) - p(i, 2), '.k')
hold off
xlim([0 6])
ylim([-0.15 0.15])
ylabel('residual', 'FontSize', 12)
xlabel('magnitude', 'FontSize', 12)
title('b', 'FontSize', 14)
set(gcf, 'Color', 'w')
export_fig residuals.png