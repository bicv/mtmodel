function tuningVels = tuningVelocities()
  tuningRange = -6:0.2:6;
  tuningVels = zeros(length(tuningRange)^2, 2);
  tune = 1;
  for y = tuningRange
    for x = tuningRange
      tuningVels(tune, :) = [atan2(y, x) sqrt(x^2 + y^2)];
      tune = tune + 1;
    end
  end
end