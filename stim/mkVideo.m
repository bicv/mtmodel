% Make a grayscale "video" matrix from a video file or folder of images
function s = mkVideo(video, frames)
  % If a folder
  if isdir(video)
    files = dir(video);
    numImages = 1;
    for file = files'
      [pathstr, name, ext] = fileparts(file.name);
      % If an image
      if strcmp(ext, '.png') | strcmp(ext, '.jpg')
        % Concatenate to stimulus
        s(:, :, numImages) = double(imread(strcat(video, '/', name, ext)));
        numImages = numImages + 1;
      end
    end
  else
    % Read file
    vidObj = VideoReader(video);
    % Extract properties
    if nargin == 2
      % Extract the specified number of frames
      if frames > vidObj.NumberOfFrames
        frames = vidObj.NumberOfFrames;
      end
      nFrames = frames;
    else
      nFrames = vidObj.NumberOfFrames;
    end
    vidHeight = vidObj.Height;
    vidWidth = vidObj.Width;
    % Allocate grayscale video
    s = zeros(vidHeight, vidWidth, nFrames);
    colorVid = read(vidObj);
    % Convert every frame into grayscale
    parfor f = 1:nFrames
      J = rgb2gray(colorVid(:, :, :, f));
      s(:, :, f) = J;
    end
  end
end