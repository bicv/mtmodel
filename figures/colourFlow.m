function colourFlow(flow)
  % Plot flow
  subaxis(3, 4, [2:4 6:8 10:12], 'Margin', 0.08, 'SpacingVert', 0, 'SpacingHoriz', 0.005)
  subimage(flowToColor(flow))
  axis off
  subaxis(3, 4, 5)
  % Plot colourwheel
  mag = sqrt(flow(:, :, 1).^2 + flow(:, :, 2).^2);
  maxFlow = ceil(max(mag(:)));
  velocityRange = -maxFlow:maxFlow/100:maxFlow;
  [X, Y] = meshgrid(velocityRange);
  col = zeros(length(velocityRange), length(velocityRange), 2);
  col(:, :, 1) = X;
  col(:, :, 2) = Y;
  subimage(flowToColor(col, maxFlow))
  set(gca, 'YTick', [1 101 201], 'YTickLabel', [-maxFlow 0 maxFlow], 'XTick', [1 101 201], 'XTickLabel', [-maxFlow 0 maxFlow])
  xlabel('V_x')
  ylabel('V_y')
  set(gcf, 'Color', 'w')
end