% Generates training data for a neural network
function [X, y] = genTestStimuli(tuningVels)
  % Number of times to create data
  iters = 3;
  % Create model using stimuli
  pars = shPars;
  pars.v1SpatialFilters = pars.v1SpatialFilters(2:8, :);
  pars.v1TemporalFilters = pars.v1TemporalFilters(2:8, :);
  % Stimuli parameters
  stim = 'dots';
  dirs = 0:0.2:2*pi;
  speeds = 0:0.2:6;
  X = zeros(iters * length(dirs) * length(speeds), size(tuningVels, 1));
  y = zeros(iters * length(dirs) * length(speeds), 2);
  m = 1;
  for iter = 1:iters
    for dir = dirs
      for speed = speeds
        s = genStimulus(stim, [35, 35, 7], dir, speed);
        % Pattern-selective cells
        [~, ind, res] = shModel(s, pars, 'mtPattern', tuningVels);
        % Retrieve responses
        X(m, :) = shGetNeuron(res, ind)';
        y(m, :) = [speed*cos(dir) speed*sin(dir)];
        m = m + 1;
      end
    end
  end
  % Shuffle examples
  shuffle = randperm(size(X, 1));
  X = X(shuffle, :);
  y = y(shuffle, :);
end