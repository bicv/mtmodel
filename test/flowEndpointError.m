% Compute the flow endpoint error between estimated flow and ground truth
function ee = flowEndpointError(flow, flowGT)
  % Extract vectors
  u = flow(:, :, 1);
  v = flow(:, :, 2);
  uGT = flowGT(:, :, 1);
  vGT = flowGT(:, :, 2);
  % Calculate error
  ee = sqrt((u - uGT).^2 + (v - vGT).^2);
end