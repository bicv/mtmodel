% Plot neuron response curves
function timeResponse(type, p, stim, dir, speed, pars)
  % Plot by neuron type
  if strcmp(type, 'v1')
    figure;
    [~, sortedInd] = sort(mean(p, 2), 1, 'descend'); % Sort by mean response
    plot([0 0], [1 1], 'w') % Dummy point, for legend title
    hold on
    plot(p(sortedInd,:)')
    hold off
    set(gca, 'XLim', [1 size(p, 2)])
    xlabel('time (frames)')
    ylabel('response (arb. units)')
    title(sprintf('V1 responses to %s moving %.1f rad at %.1f pixels/frame', stim, dir, speed))
    dirs = pars.v1PopulationDirections;
    ndirs= 5;
    eval(['legend(gca,''pref. [dir, temp/spat]''',sprintf(',''[%.1f, %.1f]''', dirs(sortedInd(1:ndirs),:)'), ')']);
  elseif strcmp(type, 'mt')
    figure;
    [~, sortedInd] = sort(mean(p, 2), 1, 'descend'); % Sort by mean response
    plot([0 0], [1 1], 'w') % Dummy point, for legend title
    hold on
    plot(p(sortedInd,:)')
    hold off
    set(gca, 'XLim', [1 size(p, 2)])
    xlabel('time (frames)')
    ylabel('response (arb. units)')
    title(sprintf('MT responses to %s moving at %.1f rad at %.1f pixels/frame', stim, dir, speed))
    vels = pars.mtPopulationVelocities;
    nvels = 5;
    eval(['legend(gca,''pref. [dir, speed]''',sprintf(',''[%.1f, %.1f]''', vels(sortedInd(1:nvels),:)'), ')']);
  end
end