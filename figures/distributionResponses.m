% Create stimulus moving at various speeds
stim = 'plaid';
dir = 0;
speed = 0;
s = genStimulus(stim, [50, 50, 7], dir, speed);
%showFrames(s)
% Create model using stimuli
pars = shPars;
pars.v1SpatialFilters = pars.v1SpatialFilters(2:8, :);
pars.v1TemporalFilters = pars.v1TemporalFilters(2:8, :);
% Pattern-selective cells
tuningRange = -6:0.2:6;
tuningVels = tuningVelocities();
[pop, ind, res] = shModel(s, pars, 'mtPattern', tuningVels);
% Retrieve responses
pmt = shGetNeuron(res, ind);
% Plot
pmt = rot90(reshape(pmt, length(tuningRange), length(tuningRange)));
imagesc(pmt)
colormap(gray)
set(gca, 'Ydir', 'normal')
tickLabels = -6:6;
ticks = linspace(1, length(tuningRange), length(tickLabels));
set(gca, 'YTick', ticks, 'YTickLabel', tickLabels, 'XTick', ticks, 'XTickLabel', tickLabels)
xlabel('V_x', 'FontSize', 12)
ylabel('V_y', 'FontSize', 12)
axis square
colorbar
set(gcf, 'Color', 'w')
export_fig(sprintf('%s%ddist.png', stim, speed))