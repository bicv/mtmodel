% Calculate error statistics
function [av, sd, rx] = errorStats(type, err)
  av = mean(mean(err));
  sd = std(std(err));
  rx = zeros(3, 1);
  total = size(err, 1) * size(err, 2);
  if strcmp(type, 'AE')
    % R2.5
    rx(1) = sum(sum(err > 2.5)) / total;
    % R5.0
    rx(2) = sum(sum(err > 5)) / total;
    % R10.0
    rx(3) = sum(sum(err > 10)) / total;
  elseif strcmp(type, 'EE')
    % R0.5
    rx(1) = sum(sum(err > 0.5)) / total;
    % R1.0
    rx(2) = sum(sum(err > 1)) / total;
    % R2.0
    rx(3) = sum(sum(err > 2)) / total;
  end
end