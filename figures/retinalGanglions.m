figure
subplot(1, 2, 1)
hold on
rectangle('Position', [0, 0, 10, 10], 'Curvature', [1, 1], 'FaceColor', 'k', 'LineStyle', 'none')
rectangle('Position', [3, 3, 4, 4], 'Curvature', [1, 1], 'FaceColor', [0.9 0.9 0.9], 'LineStyle', 'none')
hold off
axis square
axis off
title('on-centre receptive field', 'FontSize', 14)
subplot(1, 2, 2)
hold on
rectangle('Position', [0, 0, 10, 10], 'Curvature', [1, 1], 'FaceColor', [0.9 0.9 0.9], 'LineStyle', 'none')
rectangle('Position', [3, 3, 4, 4], 'Curvature', [1, 1], 'FaceColor', 'k', 'LineStyle', 'none')
hold off
axis square
axis off
title('off-centre receptive field', 'FontSize', 14)
set(gcf, 'Color', 'w')
export_fig retinalGanglions.png