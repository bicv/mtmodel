% Generate different types of stimuli
function s = genStimulus(type, size, dir, speed)
  if strcmp(type, 'sine')
    gratingParams = v12sin([dir, speed]);
    spatialFrequency = gratingParams(2);
    temporalFrequency = gratingParams(3);
    s = mkSin(size, dir, spatialFrequency, temporalFrequency);
  elseif strcmp(type, 'plaid')
    gratingParams = v12sin([dir, speed]);
    spatialFrequency = gratingParams(2);
    temporalFrequency = gratingParams(3);
    s = mkPlaid(size, dir, spatialFrequency, temporalFrequency);
  elseif strcmp(type, 'dots')
    s = mkDots(size, dir, speed, 0.15);
  end
end