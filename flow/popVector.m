% Infer a stimuli using a population vector code
function vel = popVector(resp, tuningVels)
  T = size(resp, 2);
  % Convert velocities from polar to Cartesian
  vels = [tuningVels(:, 2).*cos(tuningVels(:, 1)) tuningVels(:, 2).*sin(tuningVels(:, 1))];
  vel = zeros(T, 2);
  for t = 1:T
    vel(t, :) = sum(vels .* [resp(:, t) resp(:, t)]) / sum(resp(:, t));
  end
  vel(isnan(vel)) = 0;
end