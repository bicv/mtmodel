% Compute optical flow between two images using the Lucas-Kanade method
function flow = LucasKanade(I1, I2, windowSize, isWeighted)
  % Compute partial derivatives
  [dx, dy, dt] = computeDerivatives(I1, I2);
  % Allocate optical flow array
  flow = zeros(size(I1, 1), size(I1, 2), 2);
  % Iterate over all local neighbourhoods
  halfWindow = floor(windowSize / 2);  
  W = diag(mkGaussianFilter((windowSize^2 - 1) / 6));
  tic
  for i = halfWindow+1:size(dx, 1)-halfWindow
    parfor j = halfWindow+1:size(dx, 2)-halfWindow
      % Extract neighbourhood derivatives
      curDx = dx(i-halfWindow:i+halfWindow, j-halfWindow:j+halfWindow)';
      curDy = dy(i-halfWindow:i+halfWindow, j-halfWindow:j+halfWindow)';
      curDt = dt(i-halfWindow:i+halfWindow, j-halfWindow:j+halfWindow)';
      % Calculate the solution using least squares
      A = [curDx(:) curDy(:)];
      if isWeighted
        U = pinv(A' * W * A) * A' * W * -curDt(:);
      else
        U = pinv(A) * -curDt(:);
      end
      % Assign optical flow vectors
      flow(i, j, :) = U;
    end
    toc
    i
  end
end   