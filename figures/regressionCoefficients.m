% Extract Beta coefficients (requires Beta)
n = sqrt(size(Beta, 1));
Betax = reshape(Beta(:, 1), n, n);
Betay = reshape(Beta(:, 2), n, n);
% Plot
imagesc(Betax)
set(gca, 'Ydir', 'normal')
tickLabels = -6:6;
ticks = linspace(1, n, length(tickLabels));
set(gca, 'YTick', ticks, 'YTickLabel', tickLabels, 'XTick', ticks, 'XTickLabel', tickLabels)
xlabel('V_x', 'FontSize', 12)
ylabel('V_y', 'FontSize', 12)
axis square
colorbar
set(gcf, 'Color', 'w')
export_fig Betax.png