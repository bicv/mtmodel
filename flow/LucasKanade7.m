% Compute optical flow between seven images using the Lucas-Kanade method
function flow = LucasKanade7(s, windowSize)
  % Compute partial derivatives with finite difference coefficient mask
  gradMask = [-1 9 -45 0 45 -9 1]/60;
  normWeight = normpdf(-1.5:0.5:1.5);
  normWeight = normWeight' * normWeight;
  gradFilter = bsxfun(@times, normWeight, permute(gradMask, [3 1 2]));
  %gradFilter = gradMask' * normWeight;
  % Convolve with gradient filter
  dx = convn(s, permute(gradFilter, [2 3 1]), 'same');
  dy = convn(s, permute(gradFilter, [3 1 2]), 'same');
  dt = convn(s, gradFilter, 'same');
  % Allocate optical flow array
  flow = zeros(size(s, 1), size(s, 2), 2);
  % Iterate over all local neighbourhoods
  halfWindow = floor(windowSize / 2);
  tic
  for i = halfWindow+1:size(dx, 1)-halfWindow
    parfor j = halfWindow+1:size(dx, 2)-halfWindow
      % Extract neighbourhood derivatives
      curDx = dx(i-halfWindow:i+halfWindow, j-halfWindow:j+halfWindow)';
      curDy = dy(i-halfWindow:i+halfWindow, j-halfWindow:j+halfWindow)';
      curDt = dt(i-halfWindow:i+halfWindow, j-halfWindow:j+halfWindow)';
      % Calculate the solution using least squares
      A = [curDx(:) curDy(:)];
      U = pinv(A) * -curDt(:);
      % Assign optical flow vectors
      flow(i, j, :) = U;
    end
    toc
    i
  end
end