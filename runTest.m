% Script for running tests automatically

% Setup
setup;
pars = shPars;
pars.v1SpatialFilters = pars.v1SpatialFilters(2:8, :);
pars.v1TemporalFilters = pars.v1TemporalFilters(2:8, :);
dims = shGetDims(pars, 'mtPattern');
border = floor(dims(1) / 2);

% Moving circle stimulus
%{
s = zeros(100, 100, 7);
for i = 1:7
  s(:, :, i) = mkWin([100 100 1], 40, [0 0], [i*10 50 1]);
end
%}
% Generate stimulus out of Grove2, Grove3, Hydrangea, RubberWhale, Urban2, Urban3
path = 'data/Urban2';
s = mkVideo(path);
% Blur image
%{
for i = 1:size(s, 3)
  s(:, :, i) = imfilter(s(:, :, i), fspecial('gaussian', [35 35], 5));
end
%}
% Read ground truth
ground = readFlowFile(strcat(path, '/flow10.flo'));

% Compute optical flow
% Lucas-Kanade (L-K, WWL-K, L-K7)
flow = opticalFlow(s(:, :, 1:7), 'L-K7');
% MT
%{
resp = opticalFlow(s(:, :, 1:7), 'MT');
resp = reshape(resp, size(ground, 1)*size(ground, 2), size(resp, 3));
tuningVels = tuningVelocities();
flow = popVector(resp', tuningVels);
%trainNN;flow = regressionVector(resp, Beta);
flow = reshape(flow, size(ground, 1), size(ground, 2), 2);
%}

% Remove boundary on flow
flow(1:border, :, :) = [];
flow(end-border:end, :, :) = [];
flow(:, 1:border, :) = [];
flow(:, end-border:end, :) = [];
ground(1:border, :, :) = [];
ground(end-border:end, :, :) = [];
ground(:, 1:border, :) = [];
ground(:, end-border:end, :) = [];
% Remove large values
largeMask = ground >= 1e9;
ground(largeMask) = 0;

% Calculate error statistics
ae = angularError(flow, ground);
ee = flowEndpointError(flow, ground);
[avAng, sdAng, rxAng] = errorStats('AE', ae)
[avEnd, sdEnd, rxEnd] = errorStats('EE', ee)

% Quit if headless
if java.lang.System.getProperty('java.awt.headless')
  quit
end