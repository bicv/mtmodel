% Calculate the neural network (sigmoid units) cost function
function [J grad] = neuralNetworkSigmoid(X, y, layer_sizes, Theta, lambda)
  % Reshape weights
  Theta1 = reshape(Theta(1:layer_sizes(2) * (layer_sizes(1) + 1)), layer_sizes(2), (layer_sizes(1) + 1));
  Theta2 = reshape(Theta((numel(Theta1) + 1):(numel(Theta1) + layer_sizes(3) * (layer_sizes(2) + 1))), layer_sizes(3), (layer_sizes(2) + 1));
  Theta3 = reshape(Theta((numel(Theta1) + numel(Theta2) + 1):end), layer_sizes(4), (layer_sizes(3) + 1));
  % Amount of data
  m = size(X, 1);
  
  % Forward propagation and cost
  % Add bias units
  a1 = [ones(m, 1) X];
  % Hidden layer
  z2 = (Theta1 * a1')';
  a2 = [ones(m, 1) sigmoid(z2)];
  z3 = (Theta2 * a2')';
  a3 = [ones(m, 1) sigmoid(z3)];
  % Output layer
  z4 = (Theta3 * a3')';
  a4 = sigmoid(z4);
  Y = y;
  % Calculate cost
  J = 1/m * sum(sum(-Y.*log(a4) - (1-Y).*log(1-a4)));
  % Add regularization
  Theta1_reg = Theta1;
  Theta1_reg(:, 1) = 0;
  Theta2_reg = Theta2;
  Theta2_reg(:, 1) = 0;
  Theta3_reg = Theta3;
  Theta3_reg(:, 1) = 0;
  J = J + lambda/(2*m) * (sum(sum(Theta1_reg.^2)) + sum(sum(Theta2_reg.^2)) + sum(sum(Theta3_reg.^2)));
  
  % Backpropagation
  % Calculate deltas
  delta4 = a4 - Y;
  Delta3 = delta4' * a3;
  theta3_delta4 = delta4 * Theta3;
  delta3 = theta3_delta4(:, 2:end) .* sigmoidGradient(z3);
  Delta2 = delta3' * a2;
  theta2_delta3 = delta3 * Theta2;  
  delta2 = theta2_delta3(:, 2:end) .* sigmoidGradient(z2);
  Delta1 = delta2' * a1;
  % Calculate cost gradients
  Theta3_grad = 1/m * Delta3;
  Theta2_grad = 1/m * Delta2;
  Theta1_grad = 1/m * Delta1;
  % Add regularization
  Theta3_grad = Theta3_grad + lambda/m * Theta3_reg;
  Theta2_grad = Theta2_grad + lambda/m * Theta2_reg;
  Theta1_grad = Theta1_grad + lambda/m * Theta1_reg;
  
  % Unroll gradients
  grad = [Theta1_grad(:); Theta2_grad(:); Theta3_grad(:)];
end