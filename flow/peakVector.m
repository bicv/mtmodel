% Infer a stimuli using a population vector code
function vel = popVector(resp, tuningVels)
  s = sqrt(size(resp, 1));
  T = size(resp, 2);
  vel = zeros(T, 2);
  for t = 1:T
    [pk, loc] = findpeaks(resp(:, t), 'MINPEAKDISTANCE', s*2, 'SORTSTR', 'descend', 'NPEAKS', 1);
    vel(t, :) = tuningVels(loc, :);
  end
end