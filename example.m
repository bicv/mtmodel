% Generate tuning velocities
tuningVels = tuningVelocities();

% Generate and split data into training and test
if exist('learn/XyDots.mat', 'file') == 2
  load('learn/XyDots.mat')
else
  [X, y] = genTestStimuli(tuningVels);
  save('learn/XyDots.mat', 'X', 'y')
end
testInd = floor(size(X, 1) / 5) * 4;
XTrain = X(1:testInd, :);
yTrain = y(1:testInd, :);
XTest  = X(testInd+1:end, :);
yTest  = y(testInd+1:end, :);

% Predict
predPop = popVector(XTest', tuningVels);
% Calculate errors
[angPop, magPop] = evalError(predPop, yTest);
[mean(angPop); mean(magPop)]
[std(angPop); std(magPop)]