% Plot neuron selectivities
pars = shPars;
cc = jet(size(pars.mtPopulationVelocities, 1));
hold all
for i = size(pars.mtPopulationVelocities, 1):-1:1
  theta = pars.mtPopulationVelocities(i, 1);
  r = pars.mtPopulationVelocities(i, 2);
  h = plot([0 r*cos(theta)], [0 r*sin(theta)], 'Color', cc(i, :))
  set(h, 'Marker', '.')
  set(h, 'Markersize', 20)
end
hold off
ylim([-7 7])
xlim([-7 7])
axis square
ylabel('V_y', 'FontSize', 12)
xlabel('V_x', 'FontSize', 12)
legend('neuron 1', 'neuron 2', 'neuron 3', 'neuron 4', 'neuron 5', 'neuron 6', 'neuron 7', 'neuron 8', 'neuron 9', 'neuron 10', 'neuron 11', 'neuron 12', 'neuron 13', 'neuron 14', 'neuron 15', 'neuron 16', 'neuron 17', 'neuron 18', 'neuron 19', 'Location', 'EastOutside')
set(gcf, 'Color', 'w')
export_fig selectivities.png