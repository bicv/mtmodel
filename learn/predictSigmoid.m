% Predict the value of an input given weights from a neural network
function p = predict(X, Theta1, Theta2, Theta3)
  m = size(X, 1);  
  h1 = [ones(m, 1) X] * Theta1';
  h2 = sigmoid([ones(m, 1) h1] * Theta2');
  h3 = sigmoid([ones(m, 1) h2] * Theta3');
  p = h3;
end