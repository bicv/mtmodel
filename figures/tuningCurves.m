clf reset; set(gcf, 'color', 'w');
pars = shPars;

[x0, v1Zero] = shTunePlaidDirection(pars, [0 0], 'v1Complex', 21);
[x1, mtZero] = shTunePlaidDirection(pars, [0 0], 'mtPattern', 21);

[x, v1Sin] = shTuneGratingDirection(pars, [0 1], 'v1Complex', 21);
[x2, v1Plaid] = shTunePlaidDirection(pars, [0 1], 'v1Complex', 21);

[x3, mtSin] = shTuneGratingDirection(pars, [0 1], 'mtPattern', 21);
[x4, mtPlaid] = shTunePlaidDirection(pars, [0 1], 'mtPattern', 21);

axisMax = 1.25*max(v1Zero);
subplax([3 2], [3.15 1.15], [.7 .7], 1);
polar(0,axisMax); hold on; polar(x0, v1Zero, 'k.-'); hold off
title('a', 'FontSize', 12)
%
axisMax = 1.25*max(mtZero);
subplax([3 2], [3.15 2.15], [.7 .7], 1);
polar(0,axisMax); hold on; polar(x1, mtZero, 'k.-'); hold off
title('b', 'FontSize', 12)
%
axisMax = 1.25*max(v1Sin);
subplax([3 2], [2.15 1.15], [.7 .7], 1);
polar(0, axisMax); hold on; polar(x, v1Sin, 'k.-'); hold off
title('c', 'FontSize', 12)
%
axisMax = 1.25*max(mtSin);
subplax([3 2], [2.15 2.15], [.7 .7], 1);
polar(0,axisMax); hold on; polar(x, mtSin, 'k.-'); hold off
title('d', 'FontSize', 12)
%
axisMax = 1.15*max(v1Plaid);
subplax([3 2], [1.15 1.15], [.7 .7], 1);
polar(0, axisMax); hold on; polar(x, v1Plaid, 'k.-'); hold off
title('e', 'FontSize', 12)
%
axisMax = 1.25*max(mtPlaid);
subplax([3 2], [1.15 2.15], [.7 .7], 1);
polar(0,axisMax); hold on; polar(x, mtPlaid, 'k.-'); hold off
title('f', 'FontSize', 12)

export_fig tuningCurves.png