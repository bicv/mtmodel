% Compute the spatial and temporal partial derivates between 2 images
function [dx, dy, dt] = computeDerivatives(I1, I2)
  % Check inputs
  if (size(I1, 1) ~= size(I2, 1) | size(I1, 1) ~= size(I2, 1))
    error('Input images are not the same size');
  elseif (size(I1, 3) ~= 1 | size(I1, 3) ~= 1)
    error('Method only works on grayscale images');
  end
  
  % Convolve with gradient masks
  dx = conv2(I1, 0.25*[-1 1; -1 1]) + conv2(I2, 0.25*[-1 1; -1 1]);
  dy = conv2(I1, 0.25*[-1 -1; 1 1]) + conv2(I2, 0.25*[-1 -1; 1 1]);
  dt = conv2(I1, 0.25*ones(2)) + conv2(I2, -0.25*ones(2));
  
  % Remove borders from convolution
  dx = dx(1:size(dx, 1)-1, 1:size(dx, 2)-1);
  dy = dy(1:size(dy, 1)-1, 1:size(dy, 2)-1);
  dt = dt(1:size(dt, 1)-1, 1:size(dt, 2)-1);
end