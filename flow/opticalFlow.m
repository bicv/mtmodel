% Compute optical flow of a stimulus with the specified method
function flow = opticalFlow(s, method)
  % Check s is 7 frames
  T = 7;
  if (size(s, 3) ~= T)
    error('Stimulus is not 7 frames');
  end
  flow = zeros(size(s, 1), size(s, 2), 2, 1);
  % Model parameters
  pars = shPars;
  pars.v1SpatialFilters = pars.v1SpatialFilters(2:8, :);
  pars.v1TemporalFilters = pars.v1TemporalFilters(2:8, :);
  dims = shGetDims(pars, 'mtPattern');
  ySize = dims(1);
  xSize = dims(2);
  tSize = dims(3);
  if strcmp(method, 'MT')
    % Check size
    if (size(s, 1) < ySize | size(s, 2) < xSize | size(s, 3) < tSize)
      error('Stimulus is not large enough for MT optical flow calculation');
    end
    % Generate tuning velocities
    tuningVels = tuningVelocities();
    % Store responses for different decoding methods
    resp = zeros(size(s, 1), size(s, 2), size(tuningVels, 1), T-tSize+1);
    % Process minimum block sizes for pixel optical flow vectors
    %indD = floor(tSize/2):tSize-floor(tSize/2)
    tic
    for m = 1:size(s, 1)+1-ySize
      parfor n = 1:size(s, 2)+1-xSize
        % Pattern-selective cells
        [~, ind, res] = shModel(s(m:m+ySize-1, n:n+xSize-1, :), pars, 'mtPattern', tuningVels);
        % Retrieve responses
        p = shGetNeuron(res, ind);
        resp(m, n, :, :) = p;
      end
      m
      toc
    end
    % Shift flow (need simple indices for parfor)
    resp = circshift(resp, [floor(ySize/2) floor(xSize/2)]);
    % Save response for different decoding methods
    save('learn/resp.mat', 'resp', '-v7.3')
    % Return response as flow
    flow = resp;
  elseif strcmp(method, 'L-K')
    flow = LucasKanade(s(:, :, 4), s(:, :, 5), ySize, false);
  elseif strcmp(method, 'WWL-K')
    flow = LucasKanade(s(:, :, 4), s(:, :, 5), ySize, true);
  elseif strcmp(method, 'L-K7')
    flow = LucasKanade7(s(:, :, 1:7), ySize);
  end
end
