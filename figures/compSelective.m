% Circle mask
[rr, cc] = meshgrid(1:100);
C = sqrt((rr - 50).^2 + (cc - 50).^2) <= 50;
% Sine grating
subplot(1, 2, 1)
stim = 'sine';
dir = 0;
speed = 16;
s = genStimulus(stim, [100, 100, 1], dir, speed);
s(~C) = max(s(:));
imagesc(s)
colormap(gray)
axis square
axis off
title('a', 'FontSize', 14)
% PLaid
subplot(1, 2, 2)
stim = 'plaid';
dir = 0;
speed = 6;
s = genStimulus(stim, [60, 60, 1], dir, speed);
s = padarray(s, [20 20], max(s(:)));
imagesc(s)
colormap(gray)
axis square
axis off
title('b', 'FontSize', 14)
set(gcf, 'Color', 'w')
export_fig compSelective.png
%{
sinGrat= zeros(100);
sinGrat(:, 2:2:100) = 2;
% Plaid
plaid = sinGrat - sinGrat';
% Circle mask
[rr, cc] = meshgrid(1:100);
C = sqrt((rr - 50).^2 + (cc - 50).^2) <= 30;
sinGrat(~C) = 2;
plaid(~C) = 2;
% Display
subplot(1, 2, 1)
subimage(sinGrat, gray(2))
axis off
title('a', 'FontSize', 14)
subplot(1, 2, 2)
subimage(plaid, gray(2))
axis off
title('b', 'FontSize', 14)
%}