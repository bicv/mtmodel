% Randomly initialize weights to small values
function W = randInitialiseWeights(in_size, out_size)
  epsilon = 0.1;
  W = rand(out_size, 1 + in_size) * 2 * epsilon - epsilon;
end