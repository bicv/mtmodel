% Generate tuning velocities
tuningVels = tuningVelocities();
numNeurons = size(tuningVels, 1);
% Layer sizes: tuning velocity neurons input, direction and velocity output
layer_sizes = [numNeurons ceil(sqrt(numNeurons)) ceil(sqrt(numNeurons)) 2];
% Regularisation parameter
lambda = 0;

% Generate and split data into training and test
if exist('learn/XyPlaid.mat', 'file') == 2
  load('learn/XyPlaid.mat')
else
  [X, y] = genTestStimuli(tuningVels);
  save('learn/Xy.mat', 'X', 'y')
end
testInd = floor(size(X, 1) / 5) * 4;
XTrain = X(1:testInd, :);
yTrain = y(1:testInd, :);
XTest  = X(testInd+1:end, :);
yTest  = y(testInd+1:end, :);

% Linear regression
Beta = pinv(XTrain' * XTrain) * XTrain' * yTrain;



% Normalise data for sigmoid neural networks
%{
yTrainNorm = (yTrain + 6) / 12;

% Randomise initial weights
Theta1 = randInitialiseWeights(layer_sizes(1), layer_sizes(2));
Theta2 = randInitialiseWeights(layer_sizes(2), layer_sizes(3));
Theta3 = randInitialiseWeights(layer_sizes(3), layer_sizes(4));
% Unroll weights
Theta = [Theta1(:); Theta2(:); Theta3(:)];

% Train linear-linear
costFunction = @(theta) neuralNetworkLinear(XTrain, yTrain, layer_sizes, theta, lambda);
options = optimset('MaxIter', 400);
[ThetaLinLin, ~] = fmincg(costFunction, Theta, options);
% Train sigmoid-sigmoid
costFunction = @(theta) neuralNetworkSigmoid(XTrain, yTrainNorm, layer_sizes, theta, lambda);
options = optimset('MaxIter', 400);
[ThetaSigSig, ~] = fmincg(costFunction, Theta, options);
% Reshape weights
ThetaLinLin1 = reshape(ThetaLinLin(1:layer_sizes(2) * (layer_sizes(1) + 1)), layer_sizes(2), (layer_sizes(1) + 1));
ThetaLinLin2 = reshape(ThetaLinLin((numel(ThetaLinLin1) + 1):(numel(ThetaLinLin1) + layer_sizes(3) * (layer_sizes(2) + 1))), layer_sizes(3), (layer_sizes(2) + 1));
ThetaLinLin3 = reshape(ThetaLinLin((numel(ThetaLinLin1) + numel(ThetaLinLin2) + 1):end), layer_sizes(4), (layer_sizes(3) + 1));
ThetaSigSig1 = reshape(ThetaSigSig(1:layer_sizes(2) * (layer_sizes(1) + 1)), layer_sizes(2), (layer_sizes(1) + 1));
ThetaSigSig2 = reshape(ThetaSigSig((numel(ThetaSigSig1) + 1):(numel(ThetaSigSig1) + layer_sizes(3) * (layer_sizes(2) + 1))), layer_sizes(3), (layer_sizes(2) + 1));
ThetaSigSig3 = reshape(ThetaSigSig((numel(ThetaSigSig1) + numel(ThetaSigSig2) + 1):end), layer_sizes(4), (layer_sizes(3) + 1));

% Predict
predLinLin = predictLinear(XTest, Theta1, Theta2, Theta3);
predSigSig = (predictSigmoid(XTest, Theta1, Theta2, Theta3) * 12) - 6;
predPop = popVector(XTest', tuningVels);
% Calculate errors
[angLinLin, magLinLin] = evalError(predLinLin, yTest);
[angSigSig, magSigSig] = evalError(predSigSig, yTest);
[angPop, magPop] = evalError(predPop, yTest);
[mean(angLinLin) mean(angSigSig) mean(angPop); mean(magLinLin) mean(magSigSig) mean(magPop)]
[std(angLinLin) std(angSigSig) std(angPop); std(magLinLin) std(magSigSig) std(magPop)]
%}



% Results
predPop = popVector(XTest', tuningVels);
predPeak = peakVector(XTest', tuningVels);
predReg = XTest * Beta;
% Evaluation
[angPop, magPop] = evalError(predPop, yTest);
[angPeak, magPeak] = evalError(predPeak, yTest);
[angReg, magReg] = evalError(predReg, yTest);
% Display by method | mean angle | std angle | mean mag | std mag
[mean(angPop) std(angPop) mean(magPop) std(magPop)]
[mean(angPeak) std(angPeak) mean(magPeak) std(magPeak)]
[mean(angReg) std(angReg) mean(magReg) std(magReg)]