% Run at the start of a new session
% Add containing folder and subfolders to path
addpath(genpath(fileparts(mfilename('fullpath'))))
% Start a parallel pool (if none exists already)
gcp;