% Quiver plot optical flow vectors
function plotFlowQuiver(flow)
  THRESHOLD = 0.5;
  dx = flow(:, :, 1);
  dy = flow(:, :, 2);
  [x, y] = meshgrid(1:size(flow, 2), 1:size(flow, 1));
  % Remove small values
  sizeGrad = sqrt(dx.^2 + dy.^2);
  mask = sizeGrad > THRESHOLD;
  quiver(x(mask), y(mask), dx(mask), dy(mask));
end