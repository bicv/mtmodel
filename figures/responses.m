close all
% Create stimulus moving right at 1 pixels/frame
stim = 'sine';
dir = 0;
speed = 1;
T = 15;
s = genStimulus(stim, [50, 50, T+6], dir, speed);
%%%export_fig frames.png
% Create model using stimuli
pars = shPars;
pars.v1SpatialFilters = pars.v1SpatialFilters(2:8, :);
pars.v1TemporalFilters = pars.v1TemporalFilters(2:8, :);
% Iterate over 100 tests
tests = 100;
pv1 = zeros(28, T, tests);
pmt = zeros(19, T, tests);
for test = 1:tests
  % Add noise
  s2 = s;
  for t = 1:T+6
    s2(:, :, t) = imnoise(s(:, :, t), 'salt & pepper');
  end
  % Directionally-selective complex cells
  [popv1, indv1] = shModel(s2, pars, 'v1Complex');
  % Pattern-selective cells
  [popmt, indmt] = shModel(s2, pars, 'mtPattern');
  % Retrieve responses
  pv1(:, :, test) = shGetNeuron(popv1, indv1);
  pmt(:, :, test) = shGetNeuron(popmt, indmt);
end
% Normalise
pv1 = pv1 / max(pv1(:));
pmt = pmt / max(pmt(:));

% Plots and colour
cc = jet(28);
plots = zeros(28+1, 1);
legends = cell(6, 1);

% Plot V1
figure
pv1Mean = mean(pv1, 3);
pv1Std = std(pv1, 0, 3);
% Sort by mean response
[~, sortedInd] = sort(mean(pv1Mean, 2), 1, 'descend');
% Dummy point, for legend title
plots(1) = plot([0 0], [1 1], 'w');
hold on
for v1 = sortedInd'
  H = shadedErrorBar(1:T, pv1Mean(v1, :), pv1Std(v1, :), {'Color', cc(v1, :)}, 1);
  plots(v1+1) = H.mainLine;
end
hold off
set(plots, 'LineWidth', 2)
set(gca, 'XLim', [1 size(pv1Mean, 2)])
set(gca, 'YLim', [0 1])
xlabel('time (frames)', 'FontSize', 12)
ylabel('response (arb. units)', 'FontSize', 12)
dirs = pars.v1PopulationDirections;
ndirs = 5;
legends{1} = 'pref. [dir, temp/spat]';
for i = 1:5
  legends{i+1} = sprintf('[%.1f, %.1f]', dirs(sortedInd(i, :)', :));
end
legend([plots(1); plots(sortedInd(1:5)+1)], legends{:})
set(gcf, 'Color', 'w')
export_fig(sprintf('%sv1.png', stim))

% Plot MT
figure
pmtMean = mean(pmt, 3);
pmtStd = std(pmt, 0, 3);
% Sort by mean response
[~, sortedInd] = sort(mean(pmtMean, 2), 1, 'descend');
% Dummy point, for legend title
plots(1) = plot([0 0], [1 1], 'w');
hold on
for mt = sortedInd'
  H = shadedErrorBar(1:T, pmtMean(mt, :), pmtStd(mt, :), {'Color', cc(mt, :)}, 1);
  plots(mt+1) = H.mainLine;
end
hold off
set(plots, 'LineWidth', 2)
set(gca, 'XLim', [1 size(pmtMean, 2)])
set(gca, 'YLim', [0 1])
xlabel('time (frames)', 'FontSize', 12)
ylabel('response (arb. units)', 'FontSize', 12)
vels = pars.mtPopulationVelocities;
nvels = 5;
legends{1} = 'pref. [dir, speed]';
for i = 1:5
  legends{i+1} = sprintf('[%.1f, %.1f]', vels(sortedInd(i, :)', :));
end
legend([plots(1); plots(sortedInd(1:5)+1)], legends{:})
set(gcf, 'Color', 'w')
export_fig(sprintf('%smt.png', stim))