gauss = @(x, mu)(1/sqrt(2*pi))*exp(-(x-mu).^2/2);

x = -5:0.1:5;
y = zeros(6, length(x));
for i = 1:2:11
  y((i+1)/2, :) = gauss(x, i-6);
end
plot(x, y, 'LineWidth', 2)
axis([-5 5 0 0.5])
set(gca, 'XTickLabel', [], 'YTickLabel', [])
xlabel('stimulus parameter', 'FontSize', 12)
ylabel('neural response', 'FontSize', 12)
legend('neuron 1', 'neuron 2', 'neuron 3', 'neuron 4', 'neuron 5', 'neuron 6', 'Location', 'SouthOutside', 'Orientation', 'Horizontal')
set(gcf, 'Color', 'w')