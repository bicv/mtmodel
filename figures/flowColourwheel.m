velocityRange = -6:0.2:6;
[X, Y] = meshgrid(velocityRange);
flow = zeros(length(velocityRange), length(velocityRange), 2);
flow(:, :, 1) = X;
flow(:, :, 2) = Y;
image(flowToColor(flow))
set(gca, 'YTick', [1 16 31 46 61], 'YTickLabel', [-6 -3 0 3 6], 'XTick', [1 16 31 46 61], 'XTickLabel', [-6 -3 0 3 6])
xlabel('V_x', 'FontSize', 12)
ylabel('V_y', 'FontSize', 12)
axis square
set(gcf, 'Color', 'w')
export_fig flowColourwheel.png
%{
M = flowToColor(flow);
X0 = size(M, 1)/2;
Y0 = size(M, 2)/2;
[Y X z] = find(M);
X = X - X0;
Y = Y - Y0;
theta = atan2(Y, X);
rho = sqrt(X.^2 + Y.^2);

% Determine the minimum and the maximum x and y values:
rmin = min(rho);
tmin = min(theta);
rmax = max(rho);
tmax = max(theta);

% Define the resolution of the grid:
rres = 61; % # of grid points for R coordinate. (change to needed binning)
tres = 61; % # of grid points for theta coordinate (change to needed binning)

F = scatteredInterpolant(rho, theta, double(z), 'nearest');

%Evaluate the interpolant at the locations (rhoi, thetai).
%The corresponding value at these locations is Zinterp:

[rhoi,thetai] = meshgrid(linspace(rmin,rmax,rres),linspace(tmin,tmax,tres));
Zinterp = F(rhoi,thetai);

subplot(1,2,1); imagesc(M) ; axis square
subplot(1,2,2); imagesc(Zinterp) ; axis square
%}