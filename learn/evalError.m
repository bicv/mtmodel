% Evaluate angle and magnitude errors of vector data
function [ang, mag] = evalError(pred, ground)
 ang = acos(dot(pred, ground, 2) ./ (sqrt(sum(pred.^2, 2)) .* sqrt(sum(ground.^2, 2))));
 ang(isnan(ang)) = 0;
 mag = abs(sqrt(sum(pred.^2, 2)) - sqrt(sum(ground.^2, 2)));
end