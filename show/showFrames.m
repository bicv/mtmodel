% Plot the first 3 frames of a stimulus
function showFrames(s)
  figure
  subplot(1, 3, 1)
  subimage(s(:, :, 1))
  axis off
  subplot(1, 3, 2)
  subimage(s(:, :, 2))
  axis off
  subplot(1, 3, 3)
  subimage(s(:, :, 3))
  axis off
  set(gcf, 'Color', 'w')
end